/**
 * Created by ginollerena on 7/7/18.
 */

import React, {createContext, Component} from 'react'

export const DataContext = React.createContext({
  phones: [],
  filter_text:'',
  sort_by:'',
  onChangeFilter: ()=>{},
  onChangeSort: ()=>{}
});

function loadPhones(cb) {
  fetch('../../phones/phones.json').then((response) => {
    return response.json();
  }).then(cb);
}

class DataProvider extends Component {


  constructor(props) {
    super(props);
    this.onChangeFilter = (filter_text) => this.setState({filter_text});
    this.onChangeSort = (sort_by) => this.setState({sort_by});

    this.state = {
      phones: [],
      filter_text:'',
      sort_by:'',
      onChangeFilter: this.onChangeFilter,
      onChangeSort: this.onChangeSort
    }

  }

  componentDidMount(){
    loadPhones((phones)=>{
      console.log(phones);
      this.setState({phones})
    });
  }

  render() {
    return (
        <DataContext.Provider value={this.state}>
          {this.props.children}
        </DataContext.Provider>
    )
  }
}

export default DataProvider;