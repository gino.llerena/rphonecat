import React,{Component} from "react";
import { Link } from 'react-router-dom'


const Phone = (props) =>{

    const {phone} = props;

    return (
        <li className="thumbnail phone-listing">
          <Link to={`/phones/${phone.id}`} className="thumb" >
            <img src={phone.imageUrl} />
          </Link>
          <Link to={`/phone/${phone.id}`} >
            {phone.name}
          </Link>
          <p>{phone.snippet}</p>
        </li>
    )
}

class PhonesList extends Component{

  render() {
    const {phones} = this.props;
    console.log('PhonesList', phones);
    const list = phones.map((phone, i)=> <Phone phone={phone} key={i} />);
        
    return (
      <ul className="phones">
        {list}
      </ul>
    )
  }
};

export default PhonesList;