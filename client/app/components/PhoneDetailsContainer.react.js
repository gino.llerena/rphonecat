import React,{Component} from "react";
import PhoneDetails from "./PhoneDetails.react.js"

function getPhone(phoneId, cb) {
  fetch('/phones/' + phoneId + '.json').then((response) => {
    return response.json();
  }).then(cb);
}

class PhoneDetailsContainer extends  Component{

  constructor(props){
    super(props);
    this.state = {
      phone: null,
      active_image: 0
    }
  }

  onSelectImage = (active_image) => {
    this.setState({active_image})
  }

  componentDidMount(){
    console.log('Details', this.props);
    const {phoneId} = this.props.match.params;
    getPhone(phoneId,(phone)=> this.setState({phone}));
  }

  render() {
    const {phone, active_image} = this.state;
    return (<PhoneDetails phone={phone} active_image={active_image} onSelectImage={this.onSelectImage} />)
  }
};

export default PhoneDetailsContainer;