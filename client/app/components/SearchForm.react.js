import React,{Component} from "react";

class SearchForm extends Component{

	render () {
		const{onChangeSort, onChangeFilter} = this.props;
		return (
			<div>
		        Search:
		        <input type="text"
										onChange={(e)=> onChangeFilter(e.target.value)} />
		        Sort by:
		        <select onChange={(e)=> onChangeSort(e.target.value)}>
		          <option value="name">Alphabetical</option>
		          <option value="age">Newest</option>
		        </select>
		      </div>
		)
	}
};

export default SearchForm;
