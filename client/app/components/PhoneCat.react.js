
import React,{Component} from "react";

import PhonesList from "./PhoneList.react.js"
import SearchForm from "./SearchForm.react.js"

class PhoneCat extends Component{
  render() {
    const {phones = [], filter_text, sort_by, onChangeFilter, onChangeSort} = this.props;
    return (
      <div className="row">
        <div className="col-md-2">
          <SearchForm filter_text={filter_text} sort_by={sort_by} onChangeFilter={onChangeFilter} onChangeSort={onChangeSort} />
        </div>
        <div className="col-md-10">
          <PhonesList phones={phones} />
        </div>
      </div>
    )
  }
};

export default PhoneCat;
