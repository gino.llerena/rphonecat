
import React,{Component} from "react";
import PhoneCat from "./PhoneCat.react.js"
import {DataContext} from "../Provider.react"

function getPhones(phones, filter_text){
  return phones.filter(phone => phone.name.includes(filter_text));
}

function sortBy(sort_by, phones){
  if(sort_by === 'name'){
    return phones.sort((a,b)=> (a.name > b.name) ? 1 : -1)
  } else{
    return phones.sort((a,b)=> a.age - b.age)
  }
}

class PhoneCatContainer extends Component{

  render() {
    return (
      <DataContext.Consumer>
        {({phones, filter_text, sort_by, onChangeFilter, onChangeSort}) => (
        <PhoneCat phones={sortBy(sort_by, getPhones(phones, filter_text))}
                  filter_text={filter_text}
                  sort_by={sort_by}
                  onChangeFilter={onChangeFilter}
                  onChangeSort={onChangeSort} />
        )}
      </DataContext.Consumer>
    )
  }

};

export default PhoneCatContainer;
