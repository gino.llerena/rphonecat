
import React, {Component, createContext} from 'react'
import ReactDOM from 'react-dom'
import DataProvider from "./Provider.react"
import PhoneCatContainer from "./components/PhoneCatContainer.react"
import PhoneDetailsContainer from "./components/PhoneDetailsContainer.react"
import { withRouter } from 'react-router-dom'

import { BrowserRouter, Route, Link } from 'react-router-dom'

require('../assets/styles/style.less');

class Home extends Component {

  componentDidMount(){
     const {history} = this.props;
     history.push('/phones');
  }

  render(){
    return(
        <ul>
          <li><Link to="/phones">Home</Link></li>
        </ul>
    )
  }
}

const MyHome = withRouter(Home);


class App extends Component {

  constructor(props){
    super(props);
  }

  render() {
    return(
          <DataProvider>
            <BrowserRouter>
              <div>
                <MyHome />
                <Route path="/phones" component={PhoneCatContainer}/>
                <Route path="/phone/:phoneId" component={PhoneDetailsContainer}/>
              </div>
            </BrowserRouter>
          </DataProvider>)
  }

}

window.onload = function(){
  try{
    ReactDOM.render(<App />, document.getElementById('app-container'));
  }catch(e){
    console.log(e.stack);
  }
};