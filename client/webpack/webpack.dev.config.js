var _ = require('lodash');
var scripts = require('./scripts')
var baseConfig = require('./base.config.js')
var path = require('path');
var webpack = require('webpack');


var config = _.merge(baseConfig, {
    entry: _.merge({
        bundle: './app/main.react.js'
    }, scripts.chunks),
   output: {
        path: path.resolve(__dirname, '../../build'),
        publicPath: 'build/',
        filename: '[name].js',
        chunkFilename: 'chunk.[id].js',
        pathinfo: true,
    },
    devServer: {
        contentBase: 'web',
        devtool: 'eval',
        port: 14602,
        hot: true,
        inline: true
    },
    devtool: 'cheap-module-source-map',
    //devtool: 'eval',

});

module.exports = config ;